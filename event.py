import json
import datetime

class Event:

    def __init__(self):
        json_events='{"id":1, "name":"Harbour House", "type":"walk", "created_date":1535539168, "description":"A leg stretch"}'
        json_waypoints='{"id":1,"event_id":1,"date_time":1535539168,"latitude":"-34.0761974","longitude":"18.43831739999996","device":"browser"}' #,{"id":2,"event_id":1,"date_time":1535539168,"latitude":"-.0761974","longitude":"18.43831739999996","device":"browser"}'
        self.events = []
        self.events.append(json.loads(json_events))
        self.waypoints = []
        self.waypoints.append(json.loads(json_waypoints))

    def add_event(self, name, description, created_date, start_x, start_y, event_type, device):
        new_event = {}
        new_waypoint = {}
        new_event["id"] = len(self.events) + 1
        new_event["name"] = name
        new_event["description"] = description
        new_event["created_date"] = created_date
        new_event["type"] = event_type
        new_waypoint["id"] = len(self.waypoints) + 1
        new_waypoint["event_id"] = new_event["id"]
        new_waypoint["date_time"] = created_date
        new_waypoint["latitude"] = start_x
        new_waypoint["longitude"] = start_y
        new_waypoint["device"] = device
        self.events.append(new_event)
        self.waypoints.append(new_waypoint)
        return json.dumps(new_event)

    def add_waypoint(self, event_id, date_time, latitude, longitude, device):
        new_waypoint = {}
        new_waypoint["id"] = len(self.waypoints) + 1
        new_waypoint["date_time"] = date_time
        new_waypoint["event_id"] = event_id
        new_waypoint["latitude"] = latitude
        new_waypoint["longitude"] = longitude
        new_waypoint["device"] = device
        self.waypoints.append(new_waypoint)
        return json.dumps(new_waypoint)

    def update_event(self, update_x, update_y, update_time):
        #TODO - functionality for adding co-ordinates and time to an event
        pass
    
    def finish_event(self, finish_date_time, finish_x, finish_y):
        #TODO - functionality for completing an event
        pass

    def get_event(self, event_id):

        my_event = [x for x in self.events if x["id"] == int(event_id)]
        for event in my_event:
            new_event = event.copy()
        event_waypoints = ([x for x in self.waypoints if x["event_id"] == int(event_id)])
        new_event.update({'waypoints': event_waypoints})
        return new_event


    def json_list(self):
        return json.dumps(self.events)
