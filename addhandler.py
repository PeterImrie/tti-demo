import tornado.web
import event
import json

class AddEventHandler(tornado.web.RequestHandler):

    def initialize(self, events):
        self.events = events

    def get(self):

        self.write("Not initialised")


    def post(self):
        data = json.loads(self.request.body)
        name = data["name"]
        description = data["description"]
        created_date = data["created_date"]
        start_x = data["start_x"]
        start_y = data["start_y"]
        event_type = data["event_type"]
        device = data["device"]

        if device == "browser":
            self.write(self.events.add_event(name,description,created_date,start_x,start_y, event_type, device))
        else:
            self.write('{"error":"Not a browser"}')


