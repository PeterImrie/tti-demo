import tornado.web
import waypoints
import json

class AddWaypointHandler(tornado.web.RequestHandler):
    def initialize(self, events):
        self.events = events


    def post(self):
        data = json.loads(self.request.body)
        event_id = data["event_id"]
        waypoint_date = data["waypoint_date"]
        start_x = data["start_x"]
        start_y = data["start_y"]
        device = data["device"]
        if device == "browser":
            result = self.events.add_waypoint(event_id, waypoint_date, start_x, start_y, device)
            self.write(result)
        else:
            self.write('{"error":"Not a browser"}')
