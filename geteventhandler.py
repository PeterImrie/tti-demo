import tornado.web
import event
import json
from bson.objectid import ObjectId
from bson.json_util import dumps

class GetEventHandler(tornado.web.RequestHandler):
    def initialize(self, events):
        self.events = events


    def get(self):
        event_id = self.get_argument('eventId')
        result = self.events.get_event(event_id)
        self.write(dumps(result))
