import tornado.ioloop
import tornado.web
from event import Event
from addhandler import AddEventHandler
from gethandler import GetEventsHandler
from updatehandler import AddWaypointHandler
from geteventhandler import GetEventHandler

events = Event()


class MainHandler(tornado.web.RequestHandler):
    def get(self):
       self.write("Event Microservice v1")


def make_app():

    return tornado.web.Application([
        (r"/v1", MainHandler),
        (r"/api/v1/addevent", AddEventHandler, dict(events=events)),
        (r"/api/v1/getevents", GetEventsHandler, dict(events=events)),
        (r"/api/v1/geteventdata", GetEventHandler, dict(events=events)),
        (r"/api/v1/addwaypoint", AddWaypointHandler, dict(events=events)),
        ])


if __name__ == "__main__":
    app = make_app()
    app.listen(80)
    tornado.ioloop.IOLoop.current().start()
