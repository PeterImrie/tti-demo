from tornado import httpserver
from tornado import gen
from tornado.ioloop import IOLoop
import tornado.web

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('You are here at the root')

class defaultHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('GET - Welcome to the GetHandler!')

    def post(self):
        self.write('POST - Welcome to the PostHandler')

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/?", MainHandler),
            (r"/api/v1/activities/?", defaultHandler),
            (r"/api/v1/activities/[0-9][0-9][0-9][0-9]/?", defaultHandler)
        ]
        tornado.web.Application.__init__(self, handlers)

def main():

    app = Application()
    app.listen(80)
    IOLoop.instance().start()

if __name__ == '__main__':
    main()
