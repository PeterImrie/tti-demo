# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

TTI-Demo

Python 2.7 - restful API services with Tornado

API Endpoints are available here /api/v1/
The following endpoints, their actions, parameters and results are:
	
	getevents (GET) - Parameters: None -  returns all events with the following JSON: [{"created_date": 1535539168, "type": "walk", "id": 1, "name": "Harbour House"}]
	
	geteventdata (GET) - Parameters: url pararm eventID=<integer> - returns Events with associated Waypoint Data with the following JSON: {"waypoints": [{"date_time": 1535539168, "event_id": 1, "longitude": "18.43831739999996", "latitude": "-34.0761974", "device": "browser", "id": 1}], "type": "walk", "id": 1, "name": "Harbour House", "created_date": 1535539168}
																	* This is meant to do an on-the-fly calculation to insert max and min timestamps so that the event has a start and end date_time
																	* Fix this ASAP
	
	
	addevent (POST) - Parameters: Body params in JSON {	"name":"",
														"description":"",
														"created_date":"",
														"start_x":"",
														"start_y":"", 
														"event_type":"",
														"device":""
													   }
													   *For the purposes of this exercise device has to be set to "browser" otherwise the return will tell you otherwise
													   *created_date and all other dates passed in must be a timestamp
													   
													   If everything is good then the JSON return will be:
													   
													   {"description": "I saw a murder of crows", 
													   "start_y": "25.988", 
													   "start_x": "13.000", 
													   "created_date": "1547766111", 
													   "id": 2, 
													   "name": "Summer Moors"}
													   
	addwaypoint (POST) - Parameters: Body params in JSON {	"event_id":1,
															"waypoint_date":1020010,
															"start_x":19,
															"start_y":21,
															"device":"browser"
														  }
													   *For the purposes of this exercise device has to be set to "browser" otherwise the return will tell you otherwise
													   *created_date and all other dates passed in must be a timestamp
													   
														If everything is good then the JSON return will be:
													   
														{"date_time": 1020010, 
														  "event_id": 1, 
														  "longitude": 21, 
														  "latitude": 19, 
														  "device": "browser", 
														  "id": 3}
	
	The API endpoints are all non-authenticated and there is very little validation and error handling. Very far from production ready but put together in a hurry.
	
### How do I get set up? ###

* Summary of set up
	Spin up a new Ubuntu instance
	Connect via SSH and run the following:
		sudo apt-get update 
		sudo apt-get install python-pip #Really should have created a virtual environment
		pip install tornado
		pip install cerberus #I was looking forward to using Cerberus to do my validation but alas time was too short. Will need to carry on with this project to add in the validation
	
	I used the default ubuntu user and put my project in the home folder
	Git clone the repo from https://PeterImrie@bitbucket.org/PeterImrie/tti-demo.git , it should go into the tti-demo folder
	CD into tti-demo and then run sudo python api.py 
		- Yes we are running from the Python command line, no I didn't have time to set up a systemD process to run it automatically
		- This will run the Tornado web server and it will listen on Port 80
	
	#TODO setup nginX to proxy pass requests on to the Tornado web server
		
		

		
* Configuration
	I'm running on AWS so need to set the Security Group settings to allow traffic into the instance.
		Click on the security group assigned to the Instance, navigate to the inbound rules and allow HTTP and HTTPS traffic in 
			#TODO - listen on different port.
* Dependencies

* Database configuration
		For the purposes of this exercise I am going to try MongoDB

		### Install MongoDB - instructions taken from -> https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/ ###
		sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
		echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
		sudo apt-get update
		sudo apt-get install -y mongodb-org
		### Yes I am using PyMongo natively for this example, need more time to look at tornado motor for non-blocking
		### I actually scrapped using PyMongo natively and rolled my own little JSON like relational models - non-persistent and in memory. 
		pip install pymongo
		pip install tornado motor

		

### Learnings from this exercise ###

1) With the time pressures at play I should have focussed a bit more on what I know rather than jump in and try to use a bunch of unfamiliar tools to try and get the job done.
	- First time working with Tornado
	- First time working with MongoDB as well as the Motor package
		- This really sucked my time, trying to get the async operations working before I decided to trash it all and go back to in-memory files

### If I had more time ###

1) Get familiar with the suggested technologies first.
2) More design time and documenting data structures better
3) Set up an AWS API gateway and Lambda functions and tested the response times to see if there was any performance difference
4) Delivered on the Front-end
5) Packaged up a nice Docker image