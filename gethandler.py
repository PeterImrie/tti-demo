import tornado.web
import event
import json


class GetEventsHandler(tornado.web.RequestHandler):
    def initialize(self, events):
        self.events = events

    def get(self):
        self.write(self.events.json_list())
