import json

class Waypoints:

    def __init__(self):
        self.waypoints = []

    def add_waypoint(self, event_id, date_time, latitude, longitude):
        new_waypoint = {}
        new_waypoint["id"] = len(self.waypoints) + 1
        new_waypoint["event_id"] = event_id
        new_waypoint["latitude"] = latitude
        new_waypoint["longitude"] = longitude
        new_waypoint["date_time"] = date_time
        self.waypoints.append(new_waypoint)
        return json.dumps(new_waypoint)

    def json_list(self):
        return json.dumps(self.events)
